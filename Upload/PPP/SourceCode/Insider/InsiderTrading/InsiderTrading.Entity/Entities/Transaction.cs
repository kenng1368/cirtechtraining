﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsiderTrading.Entity.Entities
{
    public class Transaction
    {
        public Transaction() { }

        [ForeignKey("Ticker")]
        [MaxLength(10)]
        public string Ticker { get; set; }

        [ForeignKey("PersonName")]
        [MaxLength(50)]
        public string PersonName { get; set; }

        [ForeignKey("Position")]
        [MaxLength(50)]
        public string Position { get; set; }

        [MaxLength(20)]
        public string TransactionType { get; set; }
        public DateTime? Date { get; set; }
        public int Share { get; set; }
        public float Cost { get; set; }
        public float Values { get; set; }
        public int TotalShare { get; set; }

        public virtual Person Person { get; set; }
    }
}
