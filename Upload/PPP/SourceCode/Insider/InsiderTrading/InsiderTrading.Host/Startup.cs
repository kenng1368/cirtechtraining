﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InsiderTrading.Host.Startup))]
namespace InsiderTrading.Host
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
