﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace DAL
{
    public class Stock_DAL:DBConnect
    {
        string sqlConnectionString = chuoiKetNoi;
        public void SaveToDBStock(DataTable dt)
        {
            using (SqlConnection dbcon = new SqlConnection(sqlConnectionString))
            {
                MarketData ba = new MarketData();
                DataTable dt2 = new DataTable();
                dt2.Columns.AddRange(new DataColumn[11] { new DataColumn("Ticker"), new DataColumn("Date"), new DataColumn("Open"), new DataColumn("OpenAdjusted"), new DataColumn("Highest"), new DataColumn("HighestAdjusted"), new DataColumn("Lowest"), new DataColumn("LowestAdjusted"), new DataColumn("Closes"), new DataColumn("CloseAdjusted"), new DataColumn("TotaTradingVolumes") });
                dt2.Columns[2].DataType = typeof(double);
                dt2.Columns[3].DataType = typeof(double);
                dt2.Columns[4].DataType = typeof(double);
                dt2.Columns[5].DataType = typeof(double);
                dt2.Columns[6].DataType = typeof(double);
                dt2.Columns[7].DataType = typeof(double);
                dt2.Columns[8].DataType = typeof(double);
                dt2.Columns[9].DataType = typeof(double);
                dt2.Columns[10].DataType = typeof(float);
                dbcon.Open();
                if (dt.Columns.Count > 12)
                {
                    for (int i = 8; i <= dt.Rows.Count - 1; i++)
                    {
                        if (dt.Rows[i][0].ToString().Trim() == "")
                            break;
                        string close = dt.Rows[i][5].ToString().Trim();
                        double? cl;
                        string closeadj = dt.Rows[i][6].ToString().Trim();
                        double? cla;
                        string highet = dt.Rows[i][22].ToString().Trim();
                        double? hg;
                        string highetsadj = dt.Rows[i][23].ToString().Trim();
                        double? hga;
                        string lowest = dt.Rows[i][24].ToString().Trim();
                        double? lw;
                        string lowestadj = dt.Rows[i][25].ToString().Trim();
                        double? lwa;
                        string open = dt.Rows[i][26].ToString().Trim();
                        double? op;
                        string openadj = dt.Rows[i][27].ToString().Trim();
                        double? opa;
                        string volume = dt.Rows[i][40].ToString().Trim();
                        float? vl;

                        if (close == "")
                        {
                            cl = (double?)null;
                        }
                        else
                            cl = Convert.ToDouble(close);
                        if (closeadj == "")
                            cla = (double?)null;
                        else
                            cla = Convert.ToDouble(closeadj);
                        if (highet == "")
                            hg = (double?)null;
                        else
                            hg = Convert.ToDouble(highet);
                        if (highetsadj == "")
                            hga = (double?)null;
                        else
                            hga = Convert.ToDouble(highetsadj);
                        if (lowest == "")
                            lw = (double?)null;
                        else
                            lw = Convert.ToDouble(lowest);
                        if (lowestadj == "")
                            lwa = (double?)null;
                        else
                            lwa = Convert.ToDouble(lowestadj);
                        if (open == "")
                            op = (double?)null;
                        else
                            op = Convert.ToDouble(open);
                        if (openadj == "")
                            opa = (double?)null;
                        else
                            opa = Convert.ToDouble(openadj);
                        if (volume == "")
                            vl = (float?)null;
                        else
                            vl = float.Parse(volume);

                        dt2.Rows.Add(dt.Rows[i][0].ToString().Trim(), DateTime.ParseExact(dt.Rows[i][2].ToString().Trim(), "mm/dd/yyyy", new CultureInfo("en-US"), DateTimeStyles.None), op,opa,hg,hga,lw,lwa,cl,cla,vl);
                    }
                }
                else
                {
                    for (int i = 8; i <= dt.Rows.Count - 1; i++)
                    {
                        if (dt.Rows[i][0].ToString().Trim() == "")
                            break;
                        string close = dt.Rows[i][3].ToString().Trim();
                        double? cl;
                        string closeadj = dt.Rows[i][4].ToString().Trim();
                        double? cla;
                        if (close == "")
                        {
                            cl = (double?)null;
                        }
                        else
                            cl = Convert.ToDouble(close);
                        if (closeadj == "")
                            cla = (double?)null;
                        else
                            cla = Convert.ToDouble(closeadj);


                        dt2.Rows.Add(dt.Rows[i][0].ToString().Trim(), DateTime.ParseExact(dt.Rows[i][2].ToString().Trim(), "mm/dd/yyyy", new CultureInfo("en-US"), DateTimeStyles.None), DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, cl, cla, DBNull.Value);
                    }
                }
                SqlBulkCopy bulkCopy = new SqlBulkCopy(dbcon);
                bulkCopy.DestinationTableName = "MarketData";
                bulkCopy.WriteToServer(dt2);
                dbcon.Close();
            }
        }
    }
}
