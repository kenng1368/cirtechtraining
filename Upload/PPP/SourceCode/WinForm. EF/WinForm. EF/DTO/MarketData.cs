﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class MarketData
    {
        public string Ticker { get; set; }
        public DateTime Date { get; set; }
        public double Open { get; set; }
        public double OpenAdjusted { get; set; }
        public double Highest { get; set; }
        public double HighestAdjusted { get; set; }
        public double Lowest { get; set; }
        public double LowestAdjusted { get; set; }
        public double Close { get; set; }
        public double CloseAdjusted { get; set; }
        public float Volume { get; set; }
    }
}
