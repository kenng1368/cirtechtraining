﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsiderTrading.Entity.Entities
{
    public class Person
    {
        public  Person(){}
        [Key]
        [Column(Order =1)]
        [MaxLength(50)]
        public string PersonName { get; set; }
        [Key]
        [Column(Order = 2)]
        [MaxLength(50)]
        public string Position { get; set; }

        public ICollection<Transaction> Transaction { get; set; }

    }
}
