﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsiderTrading.Entity.Entities.Core
{
    public class EntitiesContent: DbContext
    {
        public EntitiesContent(): base("name=InsiderTradingConnectString"){
        }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.HasDefaultSchema("Admin");
            //modelBuilder.Entity<Transaction>().ToTable("");
            //modelBuilder.Entity<Transaction>().ToTable("");
            
        }
    }
}
