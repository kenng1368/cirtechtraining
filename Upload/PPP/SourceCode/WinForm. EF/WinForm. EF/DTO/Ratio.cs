﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Ratio
    {
        public string Ticker { get; set; }
        public int Year { get; set; }
        public int Quater { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
        public string Unit { get; set; }

    }
}
