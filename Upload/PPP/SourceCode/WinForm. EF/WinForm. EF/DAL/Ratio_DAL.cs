﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class Ratio_DAL:DBConnect
    {
        string sqlConnectionString = chuoiKetNoi;
        public void SaveToDBRatio(DataTable dt)
        {
            using (SqlConnection dbcon = new SqlConnection(sqlConnectionString))
            {
                Ratio ba = new Ratio();
                DataTable dt2 = new DataTable();
                dt2.Columns.AddRange(new DataColumn[6] { new DataColumn("Ticker"), new DataColumn("Year"), new DataColumn("Quater"), new DataColumn("Name"), new DataColumn("Value"), new DataColumn("Unit") });
                dt2.Columns[4].DataType = typeof(double);
                dbcon.Open();
                //Ratios
                for (int i = 8; i <= dt.Rows.Count - 1; i++)
                {
                    for (int j = 4; j <= dt.Columns.Count - 1; j++)
                    {
                        if (dt.Rows[i][1].ToString().Trim() == "")
                            break;
                        string pattern = dt.Rows[6][j].ToString();
                        string[] st = pattern.Split(new string[] { "Year:", "Quarter:", "Unit:" }, StringSplitOptions.RemoveEmptyEntries);
                        //Name
                        string name = pattern.Substring(0, pattern.IndexOf("\nTrailing"));
                        //Year
                        int startPositionYear = pattern.IndexOf("Year:") + "Year:".Length;
                        string year = pattern.Substring(startPositionYear, pattern.IndexOf("\nQuarter") - startPositionYear);
                        int years = int.Parse(year);
                        //Quater
                        int startPositionQuarter = pattern.IndexOf("Quarter") + "Quarter:".Length;
                        string quarter = pattern.Substring(startPositionQuarter, pattern.IndexOf("\nUnit") - startPositionQuarter);
                        if (quarter.Trim() == "Annual")
                            quarter = "5";
                        if (quarter.Trim() == "")
                            quarter = "0";
                        //Unit
                        int startUnitPosition = pattern.IndexOf("Unit:") + "Unit:".Length;
                        string unit = pattern.Substring(startUnitPosition, pattern.Length - startUnitPosition);
                        if (true == (unit.Contains("\n")))
                            unit = unit.Substring(0, unit.Length);

                        if (dt.Rows[i][j].ToString().Trim() == "")
                            dt2.Rows.Add(dt.Rows[i][1].ToString().Trim(), Convert.ToInt32(years), Convert.ToInt32(quarter), name.ToString().Trim(), DBNull.Value, unit.ToString().Trim());
                        else
                            dt2.Rows.Add(dt.Rows[i][1].ToString().Trim(), Convert.ToInt32(years), Convert.ToInt32(quarter), name.ToString().Trim(), dt.Rows[i][j].ToString().Trim(), unit.ToString().Trim());
                    }
                }
                SqlBulkCopy bulkCopy = new SqlBulkCopy(dbcon);
                bulkCopy.DestinationTableName = "Ratio";
                bulkCopy.WriteToServer(dt2);
                dbcon.Close();
            }
        }
    }
}
