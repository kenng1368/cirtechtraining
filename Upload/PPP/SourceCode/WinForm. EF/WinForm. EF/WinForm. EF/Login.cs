﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForm.EF
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void click_Login(object sender, EventArgs e)
        {
            if (tbuser.Text == "" || tbpass.Text == "")
            {
                this.tt.ForeColor = Color.Red;
                this.tt.Text = "Bạn chưa nhập tên hoặc mật khẩu! Vui lòng thử lại";
            }
            else
            {
                string username = ConfigurationManager.AppSettings["username"];
                string password = ConfigurationManager.AppSettings["password"];

                if (tbuser.Text.Trim() == username && tbpass.Text == password)
                {
                    //MessageBox.Show("Login success", "Thông Báo");
                    Main mForm = new Main();
                    this.Hide();
                    mForm.ShowDialog();
                    this.Close();
                }
                else
                {
                    this.tt.ForeColor = Color.Red;
                    this.tt.Text = "Tên đăng nhập / Mật khẩu bạn đã nhập không chính xác! Vui lòng thử lại";
                    this.tbuser.Clear();
                    this.tbuser.Focus();
                    this.tbpass.Clear();
                }
            }
        }
    }
}
