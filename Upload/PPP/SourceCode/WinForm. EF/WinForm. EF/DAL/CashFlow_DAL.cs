﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class CashFlow_DAL : DBConnect
    {
        string sqlConnectionString = chuoiKetNoi;
        public void SaveToDBCashFlow(DataTable dt)
        {
            using (SqlConnection dbcon = new SqlConnection(sqlConnectionString))
            {
                DataTable dt2 = new DataTable();
                dt2.Columns.AddRange(new DataColumn[6] { new DataColumn("Ticker"), new DataColumn("Year"), new DataColumn("Quater"), new DataColumn("Name"), new DataColumn("Value"), new DataColumn("Unit") });
                dt2.Columns[4].DataType = typeof(double);
                dbcon.Open();
                for (int i = 8; i <= dt.Rows.Count - 1; i++)
                {
                    try
                    {
                        for (int j = 4; j <= dt.Columns.Count - 1; j++)
                        {
                            if (dt.Rows[i][1].ToString().Trim() == "")
                                break;
                            string pattern = dt.Rows[6][j].ToString();
                            // Get Name
                            int startPositionName = pattern.IndexOf(".") + ".".Length;
                            string patternT = pattern.Substring(startPositionName, pattern.Length - startPositionName);

                            int startPositionNameDot = patternT.IndexOf(".") + ".".Length;
                            string patternTDot = patternT.Substring(startPositionNameDot, patternT.Length - startPositionNameDot);

                            string name = patternTDot.Substring(0, patternTDot.IndexOf("Consolidated\nYear:"));

                            // Get Year
                            int startPositionYear = patternTDot.IndexOf("Consolidated\nYear:") + "Consolidated\nYear:".Length;
                            string year = patternTDot.Substring(startPositionYear, patternTDot.IndexOf("Quarter") - startPositionYear);

                            // Get Quarter
                            int startPositionQuarter = patternTDot.IndexOf("Quarter") + "Quarter:".Length;
                            string quarter = patternTDot.Substring(startPositionQuarter, patternTDot.IndexOf("Unit") - startPositionQuarter);
                            if (quarter.Trim() == "Annual")
                                quarter = "5";
                            if (quarter.Trim() == "")
                                quarter = "0";
                            //Get unit
                            int startPositionUnit = patternTDot.IndexOf("Unit") + "Unit:".Length;
                            string unit = patternTDot.Substring(startPositionUnit, patternTDot.Length - startPositionUnit);
                            //Annual

                            if (dt.Rows[i][j].ToString().Trim() == "")
                                dt2.Rows.Add(dt.Rows[i][1].ToString().Trim(), Convert.ToInt32(year), Convert.ToInt32(quarter), name.ToString().Trim(), DBNull.Value, unit.ToString().Trim());
                            else
                                dt2.Rows.Add(dt.Rows[i][1].ToString().Trim(), Convert.ToInt32(year), Convert.ToInt32(quarter), name.ToString().Trim(), dt.Rows[i][j].ToString().Trim(), unit.ToString().Trim());
                        }
                    }
                    catch
                    {
                    }
                }
                SqlBulkCopy bulkCopy = new SqlBulkCopy(dbcon);
                bulkCopy.DestinationTableName = "CashFlow";
                bulkCopy.WriteToServer(dt2);
                dbcon.Close();
            }

        }
    }
}
