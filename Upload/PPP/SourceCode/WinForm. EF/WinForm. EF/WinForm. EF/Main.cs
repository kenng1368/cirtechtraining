﻿using Excel;
using Microsoft.Office.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;

namespace WinForm.EF
{
    public partial class Main : Form
    {

        Balance_DAL ba = new Balance_DAL();
        Ratio_DAL ra = new Ratio_DAL();
        Stock_DAL st = new Stock_DAL();
        Income_DAL ic = new Income_DAL();
        CashFlow_DAL cf = new CashFlow_DAL();
        Note_DAL nt = new Note_DAL();
        Excel_DAL ex = new Excel_DAL();
        string sqlConnectionString = Balance_DAL.chuoiKetNoi;

        string pathIn = "", pathOut = "";
        string pathTempRatios = "", pathTempBalance = "", pathTempStock = "";
        string pathTempIncome = "", pathTempBasicInfo = "";
        string pathTempCashFlow = "", pathTempNote = "";
        DataSet dsSource = null;

        public Main()
        {
            InitializeComponent();
            // Path Input
            pathIn = System.Windows.Forms.Application.StartupPath + "\\Input\\";
            if (!Directory.Exists(pathIn))
            {
                Directory.CreateDirectory(pathIn);
            }
            tbInput.Text = pathIn;
            // Path Output
            string pathOut1 = System.Windows.Forms.Application.StartupPath + "\\Output";
            pathOut = pathOut1 + "\\";
            if (!Directory.Exists(pathOut))
            {
                Directory.CreateDirectory(pathOut);
            }
            tbOutput.Text = pathOut1;

            //Path Temp Ratios
            pathTempRatios = System.Windows.Forms.Application.StartupPath + "\\Output\\Ratios\\Temp\\";
            if (!Directory.Exists(pathTempRatios))
            {
                Directory.CreateDirectory(pathTempRatios);
            }
            //Path Temp Balance
            pathTempBalance = System.Windows.Forms.Application.StartupPath + "\\Output\\Balance\\Temp\\";
            if (!Directory.Exists(pathTempBalance))
            {
                Directory.CreateDirectory(pathTempBalance);
            }
            //Path Temp Stock Market Data
            pathTempStock = System.Windows.Forms.Application.StartupPath + "\\Output\\Stock\\Temp\\";
            if (!Directory.Exists(pathTempStock))
            {
                Directory.CreateDirectory(pathTempStock);
            }
            // Path TempIncome
            pathTempIncome = System.Windows.Forms.Application.StartupPath + "\\Output\\Income\\Temp\\";
            if (!Directory.Exists(pathTempIncome))
            {
                Directory.CreateDirectory(pathTempIncome);
            }
            //Path Temp CashFlow
            pathTempCashFlow = System.Windows.Forms.Application.StartupPath + "\\Output\\CashFlow\\Temp\\";
            if (!Directory.Exists(pathTempCashFlow))
            {
                Directory.CreateDirectory(pathTempCashFlow);
            }
            //Path Temp Note
            pathTempNote = System.Windows.Forms.Application.StartupPath + "\\Output\\Note\\Temp\\";
            if (!Directory.Exists(pathTempNote))
            {
                Directory.CreateDirectory(pathTempNote);
            }
        }

        #region Choose Link In + Out
        private void click_Input(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
                tbInput.Text = fbd.SelectedPath + "\\";
        }
        private void click_Output(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
                tbOutput.Text = fbd.SelectedPath + "\\";
        }
        #endregion

        #region Click Balance
        private void click_Balance(object sender, EventArgs e)
        {
            txtStatus.Text = "";
            btnBalance.Text = "Running...";
            btnBalance.BackColor = Color.Gray;
            txtStatus.Text = "Loading...";
            MoveToTempBalance();
            ReadExcelAndSaveBalance();
            txtStatus.Text = "Done!";
            btnBalance.Text = "Balance";
            btnBalance.BackColor = Color.White;
        }
        #endregion
        #region Click Ratio
        private void click_Ratio(object sender, EventArgs e)
        {
            txtStatus.Text = "";
            btnRatios.Text = "Running...";
            btnRatios.BackColor = Color.Gray;
            txtStatus.Text = "Loading...";
            MoveToTempRatios();
            ReadExcelAndSaveRatios();
            txtStatus.Text = "Done!";
            btnRatios.Text = "Ratios";
            btnRatios.BackColor = Color.White;
        }
        #endregion
        #region Click Stock
        private void click_Stock(object sender, EventArgs e)
        {
            txtStatus.Text = "";
            btnStock.Text = "Running...";
            btnStock.BackColor = Color.Gray;
            txtStatus.Text = "Loading...";
            MoveToTempStock();
            ReadExcelAndSaveStock();
            txtStatus.Text = "Done!";
            btnStock.Text = "Stock";
        }
        #endregion
        #region Click  Income
        private void btnIncome_Click(object sender, EventArgs e)
        {
            txtStatus.Text = "";
            btnIncome.Text = "Running...";
            btnIncome.BackColor = Color.Gray;
            txtStatus.Text = "Loading...";
            MoveToTempIncome();
            ReadExcelAndSaveIncome();
            txtStatus.Text = "Done!";
            btnIncome.Text = "InCome";
        }
        #endregion
        #region Click CashFollow
        private void bntCashFlow_Click(object sender, EventArgs e)
        {
            txtStatus.Text = "";
            bntCashFlow.Text = "Running...";
            bntCashFlow.BackColor = Color.Gray;
            txtStatus.Text = "Loading...";
            MoveToTempCashFlow();
            ReadExcelAndSaveCashFlow();
            txtStatus.Text = "Done!";
            bntCashFlow.Text = "CashFlow";
        }
        #endregion
        #region Click Note
        private void btnNote_Click(object sender, EventArgs e)
        {
            txtStatus.Text = "";
            btnNote.Text = "Running...";
            btnNote.BackColor = Color.Gray;
            txtStatus.Text = "Loading...";
            MoveToTempNote();
            ReadExcelAndSaveNote();
            txtStatus.Text = "Done!";
            btnNote.Text = "Note";
        }
        #endregion

        #region Move File To Temp Balance
        public void MoveToTempBalance()
        {
            // Chuyển file sang folder Temp
            var files = Directory.GetFiles(tbInput.Text, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls") || s.EndsWith(".xlsm") || s.EndsWith(".xlsx")).Where(f => f.Contains("balance") && !f.Contains("~$"));
            Microsoft.Office.Interop.Excel.Application excelApp = null;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = null;
            foreach (string file in files)
            {
                try
                {
                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.FileValidation = MsoFileValidationMode.msoFileValidationSkip;
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    string fileEx = Path.GetExtension(file);
                    string FullNameIn = tbInput.Text + fileName + fileEx;
                    string fullNameIn_In_Temp = pathTempBalance + fileName.Replace(".", string.Empty) + ".xls";
                    if (!File.Exists(fullNameIn_In_Temp))
                    {
                        excelWorkbook = excelApp.Workbooks.Open(FullNameIn, 1, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, null, false);
                        excelApp.DisplayAlerts = false;
                        string fileNameOut = pathTempBalance + fileName.Replace(".", string.Empty);
                        excelWorkbook.SaveAs(fileNameOut, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (excelWorkbook != null)
                    {
                        Marshal.FinalReleaseComObject(excelWorkbook);
                        excelWorkbook = null;
                    }
                    if (excelApp != null)
                    {
                        excelApp.Quit();
                        Marshal.FinalReleaseComObject(excelApp);
                        excelApp = null;
                    }
                }
            }
        }
        #endregion
        #region Movie File To Temp Ratio
        public void MoveToTempRatios()
        {
            // Chuyển file sang folder Temp
            var files = Directory.GetFiles(tbInput.Text, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls") || s.EndsWith(".xlsm") || s.EndsWith(".xlsx")).Where(f => f.Contains("ratio") && !f.Contains("~$"));
            Microsoft.Office.Interop.Excel.Application excelApp = null;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = null;
            foreach (string file in files)
            {
                try
                {
                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.FileValidation = MsoFileValidationMode.msoFileValidationSkip;
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    string fileEx = Path.GetExtension(file);
                    string FullNameIn = tbInput.Text + fileName + fileEx;
                    string fullNameIn_In_Temp = pathTempRatios + fileName.Replace(".", string.Empty) + ".xls";
                    if (!File.Exists(fullNameIn_In_Temp))
                    {
                        excelWorkbook = excelApp.Workbooks.Open(FullNameIn, 1, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, null, false);
                        excelApp.DisplayAlerts = false;
                        string fileNameOut = pathTempRatios + fileName.Replace(".", string.Empty);
                        excelWorkbook.SaveAs(fileNameOut, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (excelWorkbook != null)
                    {
                        Marshal.FinalReleaseComObject(excelWorkbook);
                        excelWorkbook = null;
                    }
                    if (excelApp != null)
                    {
                        excelApp.Quit();
                        Marshal.FinalReleaseComObject(excelApp);
                        excelApp = null;
                    }
                }
            }
        }
        #endregion
        #region Movie File To Temp Stock
        public void MoveToTempStock()
        {
            // Chuyển file sang folder Temp
            var files = Directory.GetFiles(tbInput.Text, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls") || s.EndsWith(".xlsm") || s.EndsWith(".xlsx")).Where(f => f.Contains("StockMarketData") && !f.Contains("~$"));
            Microsoft.Office.Interop.Excel.Application excelApp = null;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = null;
            foreach (string file in files)
            {
                try
                {
                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.FileValidation = MsoFileValidationMode.msoFileValidationSkip;
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    string fileEx = Path.GetExtension(file);
                    string FullNameIn = tbInput.Text + fileName + fileEx;
                    string fullNameIn_In_Temp = pathTempStock + fileName.Replace(".", string.Empty) + ".xls";
                    if (!File.Exists(fullNameIn_In_Temp))
                    {
                        excelWorkbook = excelApp.Workbooks.Open(FullNameIn, 1, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, null, false);
                        excelApp.DisplayAlerts = false;
                        string fileNameOut = pathTempStock + fileName.Replace(".", string.Empty);
                        excelWorkbook.SaveAs(fileNameOut, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (excelWorkbook != null)
                    {
                        Marshal.FinalReleaseComObject(excelWorkbook);
                        excelWorkbook = null;
                    }
                    if (excelApp != null)
                    {
                        excelApp.Quit();
                        Marshal.FinalReleaseComObject(excelApp);
                        excelApp = null;
                    }
                }
            }
        }
        #endregion
        #region Move File To Temp Income
        void MoveToTempIncome()
        {
            // Chuyển file sang folder Temp
            var files = Directory.GetFiles(tbInput.Text, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls") || s.EndsWith(".xlsm") || s.EndsWith(".xlsx")).Where(f => f.Contains("income") && !f.Contains("~$"));
            Microsoft.Office.Interop.Excel.Application excelApp = null;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = null;
            foreach (string file in files)
            {
                try
                {
                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.FileValidation = MsoFileValidationMode.msoFileValidationSkip;
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    string fileEx = Path.GetExtension(file);
                    string FullNameIn = tbInput.Text + fileName + fileEx;
                    string fullNameIn_In_Temp = pathTempIncome + fileName.Replace(".", string.Empty) + ".xls";
                    if (!File.Exists(fullNameIn_In_Temp))
                    {
                        excelWorkbook = excelApp.Workbooks.Open(FullNameIn, 1, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, null, false);
                        excelApp.DisplayAlerts = false;
                        string fileNameOut = pathTempIncome + fileName.Replace(".", string.Empty);
                        excelWorkbook.SaveAs(fileNameOut, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (excelWorkbook != null)
                    {
                        Marshal.FinalReleaseComObject(excelWorkbook);
                        excelWorkbook = null;
                    }
                    if (excelApp != null)
                    {
                        excelApp.Quit();
                        Marshal.FinalReleaseComObject(excelApp);
                        excelApp = null;
                    }
                }
            }
        }
        #endregion
        #region Move File To Temp CashFlow
        void MoveToTempCashFlow()
        {
            // Chuyển file sang folder Temp
            var files = Directory.GetFiles(tbInput.Text, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls") || s.EndsWith(".xlsm") || s.EndsWith(".xlsx")).Where(f => f.Contains("cashflow") && !f.Contains("~$"));
            Microsoft.Office.Interop.Excel.Application excelApp = null;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = null;
            foreach (string file in files)
            {
                try
                {
                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.FileValidation = MsoFileValidationMode.msoFileValidationSkip;
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    string fileEx = Path.GetExtension(file);
                    string FullNameIn = tbInput.Text + fileName + fileEx;
                    string fullNameIn_In_Temp = pathTempCashFlow + fileName.Replace(".", string.Empty) + ".xls";
                    if (!File.Exists(fullNameIn_In_Temp))
                    {
                        excelWorkbook = excelApp.Workbooks.Open(FullNameIn, 1, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, null, false);
                        excelApp.DisplayAlerts = false;
                        string fileNameOut = pathTempCashFlow + fileName.Replace(".", string.Empty);
                        excelWorkbook.SaveAs(fileNameOut, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (excelWorkbook != null)
                    {
                        Marshal.FinalReleaseComObject(excelWorkbook);
                        excelWorkbook = null;
                    }
                    if (excelApp != null)
                    {
                        excelApp.Quit();
                        Marshal.FinalReleaseComObject(excelApp);
                        excelApp = null;
                    }
                }
            }
        }
        #endregion
        #region Move File To Temp Note
        void MoveToTempNote()
        {
            // Chuyển file sang folder Temp
            var files = Directory.GetFiles(tbInput.Text, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls") || s.EndsWith(".xlsm") || s.EndsWith(".xlsx")).Where(f => f.Contains("note") && !f.Contains("~$"));
            Microsoft.Office.Interop.Excel.Application excelApp = null;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = null;
            foreach (string file in files)
            {
                try
                {
                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.FileValidation = MsoFileValidationMode.msoFileValidationSkip;
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    string fileEx = Path.GetExtension(file);
                    string FullNameIn = tbInput.Text + fileName + fileEx;
                    string fullNameIn_In_Temp = pathTempNote + fileName.Replace(".", string.Empty) + ".xls";
                    if (!File.Exists(fullNameIn_In_Temp))
                    {
                        excelWorkbook = excelApp.Workbooks.Open(FullNameIn, 1, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, null, false);
                        excelApp.DisplayAlerts = false;
                        string fileNameOut = pathTempNote + fileName.Replace(".", string.Empty);
                        excelWorkbook.SaveAs(fileNameOut, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (excelWorkbook != null)
                    {
                        Marshal.FinalReleaseComObject(excelWorkbook);
                        excelWorkbook = null;
                    }
                    if (excelApp != null)
                    {
                        excelApp.Quit();
                        Marshal.FinalReleaseComObject(excelApp);
                        excelApp = null;
                    }
                }
            }
        }
        #endregion

        #region ReadExcel + SaveToBalance
        void ReadExcelAndSaveBalance()
        {
            try
            {
                var files1 = Directory.GetFiles(pathTempBalance, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls"));
                foreach (string file in files1)
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(file);
                        tb1.Text = fileName + " is loading to Server";
                        string fullNameIn_In_Out = tbOutput.Text + "Balance\\" + fileName.Replace(".", string.Empty) + ".xls";
                        if (!File.Exists(fullNameIn_In_Out))
                        {
                            dsSource = ex.GetDatasetFromExcel(file);
                            foreach (System.Data.DataTable tbl in dsSource.Tables)
                            {
                                ba.SaveToDBBalance(tbl);
                                break;
                            }

                        }
                        StoreFileBalance(file);
                        tb1.Clear();
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region ReadExcel + SaveToRatio
        void ReadExcelAndSaveRatios()
        {
            try
            {
                var files1 = Directory.GetFiles(pathTempRatios, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls"));
                foreach (string file in files1)
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(file);
                        tb1.Text = fileName + " is loading to Server";
                        string fullNameIn_In_Out = tbOutput.Text + "Ratios\\" + fileName.Replace(".", string.Empty) + ".xls";
                        if (!File.Exists(fullNameIn_In_Out))
                        {
                            dsSource = ex.GetDatasetFromExcel(file);
                            foreach (System.Data.DataTable tbl in dsSource.Tables)
                            {
                                ra.SaveToDBRatio(tbl);
                                break;
                            }

                        }
                        StoreFileRatios(file);
                        tb1.Clear();
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region ReadExcel + SaveToStock
        void ReadExcelAndSaveStock()
        {
            try
            {
                var files1 = Directory.GetFiles(pathTempStock, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls"));
                foreach (string file in files1)
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(file);
                        tb1.Text = fileName + " is loading to Server";
                        string fullNameIn_In_Out = tbOutput.Text + "Stock\\" + fileName.Replace(".", string.Empty) + ".xls";
                        if (!File.Exists(fullNameIn_In_Out))
                        {
                            dsSource = ex.GetDatasetFromExcel(file);
                            foreach (System.Data.DataTable tbl in dsSource.Tables)
                            {
                                st.SaveToDBStock(tbl);
                                break;
                            }
                        }
                        StoreFileStock(file);
                        tb1.Clear();
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }


        #endregion
        #region ReadExcel + SaveToIncome
        void ReadExcelAndSaveIncome()
        {
            try
            {
                var files1 = Directory.GetFiles(pathTempIncome, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls"));
                foreach (string file in files1)
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(file);
                        tb1.Text = fileName + " is loading to Server";
                        string fullNameIn_In_Out = tbOutput.Text + "Income\\" + fileName.Replace(".", string.Empty) + ".xls";
                        if (!File.Exists(fullNameIn_In_Out))
                        {
                            dsSource = ex.GetDatasetFromExcel(file);
                            foreach (System.Data.DataTable tbl in dsSource.Tables)
                            {
                                ic.SaveToDBIncome(tbl);
                                break;
                            }
                        }
                        StoreFileIncome(file);
                        tb1.Clear();
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region ReadExcel + SaveToCashFlow
        void ReadExcelAndSaveCashFlow()
        {
            try
            {
                var files1 = Directory.GetFiles(pathTempCashFlow, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls"));
                foreach (string file in files1)
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(file);
                        tb1.Text = fileName + " is loading to Server";
                        string fullNameIn_In_Out = tbOutput.Text + "Cashflow\\" + fileName.Replace(".", string.Empty) + ".xls";
                        if (!File.Exists(fullNameIn_In_Out))
                        {
                            dsSource = ex.GetDatasetFromExcel(file);
                            foreach (System.Data.DataTable tbl in dsSource.Tables)
                            {
                                cf.SaveToDBCashFlow(tbl);
                                break;
                            }

                        }
                        StoreFileCashFlow(file);
                        tb1.Clear();
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region ReadExcel + SaveToNote
        void ReadExcelAndSaveNote()
        {
            try
            {
                var files1 = Directory.GetFiles(pathTempNote, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xls"));
                foreach (string file in files1)
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(file);
                        tb1.Text = fileName + " is loading to Server";
                        string fullNameIn_In_Out = tbOutput.Text + "note\\" + fileName.Replace(".", string.Empty) + ".xls";
                        if (!File.Exists(fullNameIn_In_Out))
                        {
                            dsSource = ex.GetDatasetFromExcel(file);
                            foreach (System.Data.DataTable tbl in dsSource.Tables)
                            {
                                nt.SaveToDBNote(tbl);
                                break;
                            }

                        }
                        StoreFileNote(file);
                        tb1.Clear();
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        #region Move File To Folder
        private void StoreFileBalance(string fileName)
        {
            if (File.Exists(fileName))
            {
                string filenameOnly = Path.GetFileName(fileName);
                if (!Directory.Exists(tbOutput.Text + "\\Balance\\"))
                {
                    Directory.CreateDirectory(tbOutput.Text + "\\Balance\\");
                }
                File.Copy(fileName, tbOutput.Text + "\\Balance\\" + filenameOnly, true);
                File.Delete(fileName);
            }
        }
        private void StoreFileRatios(string fileName)
        {
            if (File.Exists(fileName))
            {
                string filenameOnly = Path.GetFileName(fileName);
                if (!Directory.Exists(tbOutput.Text + "\\Ratios\\"))
                {
                    Directory.CreateDirectory(tbOutput.Text + "\\Ratios\\");
                }
                File.Copy(fileName, tbOutput.Text + "\\Ratios\\" + filenameOnly, true);
                File.Delete(fileName);
            }
        }
        private void StoreFileStock(string fileName)
        {
            if (File.Exists(fileName))
            {
                string filenameOnly = Path.GetFileName(fileName);
                if (!Directory.Exists(tbOutput.Text + "\\Stock\\"))
                {
                    Directory.CreateDirectory(tbOutput.Text + "\\Stock\\");
                }
                File.Copy(fileName, tbOutput.Text + "\\Stock\\" + filenameOnly, true);
                File.Delete(fileName);
            }
        }
        private void StoreFileIncome(string fileName)
        {
            if (File.Exists(fileName))
            {
                string filenameOnly = Path.GetFileName(fileName);
                if (!Directory.Exists(tbOutput.Text + "\\Income\\"))
                {
                    Directory.CreateDirectory(tbOutput.Text + "\\Income\\");
                }
                File.Copy(fileName, tbOutput.Text + "\\Income\\" + filenameOnly, true);
                File.Delete(fileName);
            }
        }
        private void StoreFileCashFlow(string fileName)
        {
            if (File.Exists(fileName))
            {
                string filenameOnly = Path.GetFileName(fileName);
                if (!Directory.Exists(tbOutput.Text + "\\cashflow\\"))
                {
                    Directory.CreateDirectory(tbOutput.Text + "\\cashflow\\");
                }
                File.Copy(fileName, tbOutput.Text + "\\cashflow\\" + filenameOnly, true);
                File.Delete(fileName);
            }
        }
        private void StoreFileNote(string fileName)
        {
            if (File.Exists(fileName))
            {
                string filenameOnly = Path.GetFileName(fileName);
                if (!Directory.Exists(tbOutput.Text + "\\Note\\"))
                {
                    Directory.CreateDirectory(tbOutput.Text + "\\Note\\");
                }
                File.Copy(fileName, tbOutput.Text + "\\Note\\" + filenameOnly, true);
                File.Delete(fileName);
            }
        }
        #endregion
    }
}
